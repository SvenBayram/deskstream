﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeskStreamServer
{
    class Server
    {
        public bool isSending = false;
        private Thread captureThread;
        private Thread listenerThread;
        private Graphics graphics;
        private Bitmap capturedImage = new Bitmap(1920, 1080, PixelFormat.Format32bppArgb);
        private string url;

        public Server(string url)
        {
            this.url = url;

            graphics = Graphics.FromImage(capturedImage);

            captureThread = new Thread(CaptureThread);
            listenerThread = new Thread(ListenThread);

            captureThread.Start();
            listenerThread.Start();
        }

        public void Stop()
        {
            captureThread.Abort();
            listenerThread.Abort();
        }

        void CaptureThread()
        {
            try
            {
                int step = 10;

                double olaf = 0d;
                DateTime date = DateTime.Now;

                while (true)
                {
                    olaf += (DateTime.Now - date).TotalMilliseconds;

                    if (olaf > step)
                    {
                        olaf -= step;
                        graphics.CopyFromScreen(0, 0, 0, 0, (new Rectangle(new Point(0, 0), new Size(1920, 1080))).Size);
                        // TODO: Den Cursor um seinen "Hotspot" verschieben, sonst werden Textcursor falsch angezeigt.
                        // TODO: Eventuell ein Bild von einem Cursor reinhauen weil nur der Wait Cursor angezeigt wird.
                        Cursor.Current.Draw(graphics, new Rectangle(Cursor.Position, Cursor.Current.Size));
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        void ListenThread()
        {
            HttpListener listener;
            HttpListenerContext context;
            HttpListenerRequest request;
            HttpListenerResponse response;
            BitmapData bitmap;

            IntPtr pointer;

            byte[] data;
            int size;

            try
            {
                listener = new HttpListener();
                listener.Prefixes.Add(url);
                listener.Start();

                while (true)
                {
                    context = listener.GetContext();
                    request = context.Request;
                    response = context.Response;

                    lock (capturedImage)
                    {
                        bitmap = capturedImage.LockBits(new Rectangle(0, 0, 1920, 1080), ImageLockMode.ReadOnly, capturedImage.PixelFormat);

                        pointer = bitmap.Scan0;

                        size = Math.Abs(bitmap.Stride) * bitmap.Height;

                        data = new byte[size];

                        System.Runtime.InteropServices.Marshal.Copy(pointer, data, 0, size);

                        capturedImage.UnlockBits(bitmap);
                    }

                    response.ContentLength64 = size; 
                    response.OutputStream.Write(data, 0, size);
                    response.OutputStream.Close();
                }
            }
            catch (ThreadAbortException)
            {

            }
        }
    }
}
