﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Windows.Forms;

namespace DeskStreamServer
{
    internal static class Program
    {
        private static NotifyIcon icon;
        private static Server server;


        private static void ItemQuit_Click(object sender, EventArgs e)
        {
            Quit();
        }

        private static void ItemTogglePause_Click(object sender, EventArgs e)
        {
            server.isSending = !server.isSending;
            ((MenuItem)sender).Text = server.isSending ? "Übertragung stoppen" : "Übertragung starten";
        }

        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(16, 16);
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmp))
            {
                g.FillEllipse(System.Drawing.Brushes.CornflowerBlue, new System.Drawing.RectangleF(0, 0, 15, 15));
            }

            icon = new NotifyIcon();
            icon.Icon = System.Drawing.Icon.FromHandle(bmp.GetHicon());
            icon.ContextMenu = new ContextMenu();

            MenuItem itemTogglePause = new MenuItem();
            itemTogglePause.Text = "Übertragung starten";
            itemTogglePause.Click += ItemTogglePause_Click;

            MenuItem itemQuit = new MenuItem();
            itemQuit.Text = "Beenden";
            itemQuit.Click += ItemQuit_Click;

            icon.ContextMenu.MenuItems.Add(itemTogglePause);
            icon.ContextMenu.MenuItems.Add(itemQuit);
            icon.Visible = true;


            server = new Server("http://localhost:26000/deskstream/");

            Application.Run();
        }

        static void Quit()
        {
            icon.Visible = false;
            Application.Exit();
            server.Stop();
        }
    }
}