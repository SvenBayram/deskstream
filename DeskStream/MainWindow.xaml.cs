﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace DeskStream
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Bitmap showp = new Bitmap(1920, 1080, PixelFormat.Format32bppArgb);
        Graphics showg;
        Thread cthread;

        public MainWindow()
        {
            InitializeComponent();
            showg = Graphics.FromImage(showp);
            cthread = new Thread(capturethread);
            cthread.Start();
        }

        void capturethread()
        {
            try
            {
                int step = 10;

                double olaf = 0d;
                DateTime date = DateTime.Now;

                while (true)
                {
                    olaf += (DateTime.Now - date).TotalMilliseconds;

                    if (olaf > step)
                    {
                        olaf -= step;
                        Capture();
                        Send();
                        //Dispatcher.Invoke(show);
                    }
                }
            }
            catch (Exception)
            {
                
            }
        }

        private void Send()
        {
            
        }

        public void show()
        {
            imagebox.Source = BitmapToImageSource(showp);
        }

        public void Capture()
        {
            showg.CopyFromScreen(0, 0, 0, 0, (new Rectangle(new System.Drawing.Point(0, 0), new System.Drawing.Size(1920, 1080))).Size);
        }
        
        BitmapSource BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }
        
        private void Window_Closed(object sender, EventArgs e)
        {
            cthread.Abort();
        }
    }
}
