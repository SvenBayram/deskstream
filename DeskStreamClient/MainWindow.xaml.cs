﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows;

namespace DeskStreamClient
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Thread rthread;
        Graphics graphics;
        Bitmap show = new Bitmap(1920, 1080, PixelFormat.Format32bppArgb);
        string url = "http://localhost:26000/deskstream/";

        public MainWindow()
        {
            InitializeComponent();

            graphics = Graphics.FromImage(show);

            rthread = new Thread(ReceiveThread);

            rthread.Start();
        }

        void ReceiveThread()
        {
            try
            {
                while (true)
                {
                    WebRequest request = WebRequest.Create(url);

                    WebResponse response = request.GetResponse();

                    Stream stream = response.GetResponseStream();

                    byte[] bytes = new byte[response.ContentLength];

                    BinaryReader reader = new BinaryReader(stream);
                    const int bufferSize = 4096;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        byte[] buffer = new byte[bufferSize];
                        int count;
                        while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                            ms.Write(buffer, 0, count);
                        bytes = ms.ToArray();
                    }
                    //stream.Read(bytes, 0, bytes.Length);

                    stream.Close();

                    BitmapData bitdat = show.LockBits(new Rectangle(0, 0, 1920, 1080), ImageLockMode.WriteOnly, show.PixelFormat);

                    IntPtr pointer = bitdat.Scan0;

                    System.Runtime.InteropServices.Marshal.Copy(bytes, 0, pointer, bytes.Length);

                    show.UnlockBits(bitdat);

                    Dispatcher.Invoke(ShowScreen);
                }
            }
            catch (ThreadAbortException)
            {

            }
        }

        void ShowScreen()
        {
            DataContext = show.BitmapToImageSource();
        }
        
        public void Stop()
        {
            rthread.Abort();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Stop();
        }
    }
}
